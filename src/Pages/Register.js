import { useState, useContext } from "react";
import "./Register.css";
import axios from "axios";
import Login from "./Login";
import { SERVER_URL } from "Pages/Globals";
import Certificate from "./Certificate/Certificate";
import { LoginContext } from "Pages/Login";

//This version supports the "print Certificate" feature. See notes in
//TakeQuis.js and Certificate.js
export default function Register() {
  const [error, setError] = useState("");
  const [x, [mode, setMode], y] = useContext(LoginContext);

  setMode("register");

  async function register(e) {
    e.preventDefault();

    const values = Object.values(e.target);

    const params = {
      userName: values[0].value,
      userEmail: values[1].value,
      userPassword: values[2].value,
    };
    const config = { headers: { "Content-Type": "application/json" } };
    const url = `${SERVER_URL}/users/create`;
    axios
      .post(url, params, config)
      .then((res) => {
        console.log(res.data.userName);
        setMode("login");
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError("User name exists already");
      });
  }

  function login() {
    console.log("login called");
    setMode("login");
  }

  function displaySelector() {
    if (mode == "register") {
      return (
        <div className="register">
          <h1>{mode}</h1>
          <form onSubmit={register}>
            <div className="my_error">{error}</div>
            <label className="form_line">
              <div className="inputLabel">Your name here:</div>
              <div className="inputValue">
                <input name="email" type="string"></input>
              </div>
            </label>
            <label className="form_line">
              <div className="inputLabel">Your email here:</div>
              <div className="inputValue">
                <input name="email" type="string"></input>
              </div>
            </label>
            <label className="form_line">
              <div className="inputLabel">Your password here:</div>
              <div className="inputValue">
                <input name="pssword" type="string"></input>
              </div>
            </label>

            <input type="submit" value="send"></input>
          </form>
          <div>
            Already Registered?
            <button onClick={login}>Login</button>{" "}
          </div>{" "}
        </div>
      );
    } else if (mode == "certificate") {
      return <Certificate></Certificate>;
    } else {
      return <Login></Login>;
    }
  }

  return displaySelector();
}
