const localhost = "http://localhost:4000";
const remote_host = "";
// export const SERVER_URL = remote_host;
export const SERVER_URL = localhost;

export function formatDate(d, day_first = true) {
  let date = new Date(d);
  var dd = date.getDate();
  var mm = date.getMonth() + 1;
  var yyyy = date.getFullYear();
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }

  return day_first
    ? (d = dd + "-" + mm + "-" + yyyy)
    : (d = yyyy + "-" + mm + "-" + dd);
}
