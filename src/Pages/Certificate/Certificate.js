import { useEffect, useContext } from "react";
import "./Certificate.css";
import { useHistory } from "react-router-dom";
import { LoginContext } from "Pages/Login";
import Login from "Pages/Login";
import Register from "Pages/Register";
import { formatDate } from "Pages/Globals";

//This is a version that prints a certificate.
//I will not continue to develop this version because I did not succeed
// to refresh the comppnent and return to the quiz after the printing
//and the display stayed on the certificate until I manualy press refresh
// Anyhow, this component here is the same like the Login and Register in
// the way that they do not cpome under the header. They all use mode state
// and they switch between them by setting the mode.
// Also for this component, the parent component (or the component that switched
// the mode to make this one appear), had to use the context from Login in order
//to set the mode and most important to store the data required for the certificate
// somewhere where the Certificate component can read it from, and that is the Login.
// This component (Certificate) is called from Login and it is wrapped with Provider
// of the context because it is not a child component of Login, and it had to
//read the data needed for the certificate from the Login.
// Regarding the sensitivity of the "Print Certificate" button, it is active only
// if the quiz was already submitted (in which case the save button is greyed out)
// either it was submitted in the past or just now after clicking the save button
//with submit checkbox ticked
//P.S. I solved the problem of refresh. The problem was that Login, Register and
//Certificate  components were using a separate useState for the mode. Now Login
//is the only component that holds the mode state and it provides its context
//explicitly to the other components.
function Certificate() {
  const [
    [the_user, setUser],
    [mode, setMode],
    [certificateData, setCertificateData],
  ] = useContext(LoginContext);

  setMode("certificate");

  useEffect(() => {}, []);

  function handleBack() {
    setMode("login");
  }

  console.log("data from certificate");
  console.log("user: ", the_user);
  console.log("certificate: ", certificateData);

  return mode == "certificate" ? (
    <div className="div1" onClick={handleBack}>
      <div className="div2">
        <span className="certificate_title">Certificate of Completion</span>
        <br />
        <br />
        <span style={{ fontSize: "25px" }}>
          <i>This is to certify that</i>
        </span>
        <br />
        <br />
        <span style={{ fontSize: "30px" }}>
          <b>{the_user}</b>
        </span>
        <br />
        <br />
        <span style={{ fontSize: "25px" }}>
          <i>has completed the quiz</i>
        </span>
        <br />
        <br />
        <span style={{ fontSize: "30px" }}>
          {certificateData.quizName}
        </span>{" "}
        <br />
        <br />
        <span style={{ fontSize: "20px" }}>
          with score of <b>{certificateData.record.recordMark}%</b>
        </span>
        <br />
        <br />
        <br />
        <br />
        <span style={{ fontSize: "25px" }}>
          <i>dated</i>
        </span>
        <br />
        <span style={{ fontSize: "30px" }}>
          {formatDate(certificateData.record.recordSubmissionDate)}
        </span>
      </div>
    </div>
  ) : mode == "login" ? (
    <Login></Login>
  ) : (
    <Register></Register>
  );
}
export default Certificate;
