import { createContext, useState, useEffect } from "react";
import "./Login.css";
import axios from "axios";
import Register from "./Register";
import Certificate from "./Certificate/Certificate";
import { SERVER_URL } from "Pages/Globals";

//This version supports the "print Certificate" feature. See notes in
//TakeQuis.js and Certificate.js
export const LoginContext = createContext();

export default function Login({ children }) {
  const loginState = useState();
  const [the_user, setUser] = loginState;
  const [error, setError] = useState("");

  const loginMode = useState("login");
  const [mode, setMode] = loginMode;
  const [stayConnected, setStayConnected] = useState("checked");

  const DataForCertificate = useState();
  const [certificateData, setCertificateData] = DataForCertificate;

  let quizUser = null;
  useEffect(() => {
    if (localStorage.quizUser) quizUser = JSON.parse(localStorage.quizUser);
    else if (sessionStorage.quizUser)
      quizUser = JSON.parse(sessionStorage.quizUser);

    setUser(quizUser ? quizUser.user : "");
  }, []);

  async function login(e) {
    e.preventDefault();

    const values = Object.values(e.target);

    const params = {
      userEmail: values[0].value,
      userPassword: values[1].value,
    };
    const url = `${SERVER_URL}/users/login`;

    axios
      .post(url, params)
      .then((res) => {
        console.log("the token is: \n", res.data.token);
        setUser(res.data.userName);
        if (stayConnected)
          localStorage.quizUser = JSON.stringify({
            user: res.data.userName,
            token: res.data.token,
          });
        else
          sessionStorage.quizUser = JSON.stringify({
            user: res.data.userName,
            token: res.data.token,
          });
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }

  function register() {
    console.log("register called");
    setMode("register");
  }

  function saveToken() {
    console.log("saveToken called");
    setStayConnected(stayConnected == "" ? "checked" : "");
  }

  function displaySelector() {
    if (mode == "login") {
      return (
        <LoginContext.Provider
          value={[loginState, loginMode, DataForCertificate]}
        >
          {the_user ? (
            children
          ) : (
            <div className="login">
              <h1>{mode}</h1>
              <form onSubmit={login}>
                <div style={{ color: "red" }}>{error}</div>
                <label className="form_line">
                  <div className="inputLabel">Your email here:</div>
                  <div className="inputValue">
                    <input name="email" type="string"></input>
                  </div>
                </label>
                <label className="form_line">
                  <div className="inputLabel">Your password here:</div>
                  <div className="inputValue">
                    <input name="pssword" type="string"></input>
                  </div>
                </label>

                <input type="submit" value="send"></input>
              </form>
              <div>
                <label>
                  <input
                    type="checkbox"
                    onChange={saveToken}
                    checked={stayConnected}
                  ></input>
                  stay connected
                </label>
              </div>
              <div>
                Not Registered?
                <button onClick={register}>Register</button>{" "}
              </div>{" "}
            </div>
          )}
        </LoginContext.Provider>
      );
    } else if (mode == "certificate") {
      return (
        <LoginContext.Provider
          value={[loginState, loginMode, DataForCertificate]}
        >
          <Certificate></Certificate>
        </LoginContext.Provider>
      );
    } else {
      return (
        <LoginContext.Provider
          value={[loginState, loginMode, DataForCertificate]}
        >
          {" "}
          <Register></Register>
        </LoginContext.Provider>
      );
    }
  }

  return displaySelector();
}
