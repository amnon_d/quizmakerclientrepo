import "./app.css";
import Login from "Pages/Login";
import Header from "Components/Header/Header";
import CreatedQuizzes from "Components/CreatedQuizzes/CreatedQuizzes";
import TookQuizzes from "Components/TookQuizzes/TookQuizzes";
import CreateQuiz from "Components/CreateQuiz/CreateQuiz";
import EditQuiz from "Components/EditQuiz/EditQuiz";
import CreateQandA from "Components/QandA/CreateQandA";
import EditQandA from "Components/QandA/EditQandA";
import TakeQuiz from "Components/TakeQuiz/TakeQuiz";
import CorrectAnswers from "Components/CorrectAnswers/CorrectAnswers";
import SendQuiz from "Components/SendQuiz/SendQuiz";

import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Login>
        <Header></Header>
        <Switch>
          <Route path="/" component={CreatedQuizzes} exact />{" "}
          <Route path="/quizzes-took" component={TookQuizzes} />{" "}
          <Route path="/quizform" component={CreateQuiz} exact />
          <Route path="/quizform/:quizId?" component={EditQuiz} />{" "}
          <Route
            path="/editqanda/:quizId?/:questionId?"
            component={EditQandA}
          />
          <Route
            path="/createqanda/:quizId?/:questionId?"
            component={CreateQandA}
          />
          <Route path="/takequiz/:quizId?" component={TakeQuiz} />
          <Route path="/correct_answers/:quizId?" component={CorrectAnswers} />
          <Route path="/send_quiz/:quizId?" component={SendQuiz} />
        </Switch>
      </Login>
    </div>
  );
}

{
  /* <Route path="/dashboard" component={Dashboard} />;
<Route path="/dashboard" component={Dashboard} isAuthed={true} />;
<Route path="/dashboard" component={() => <Dashboard isAuthed={true} />} />;

const PropsPage = ({ title }) => {
  return (
    <h3>{title}</h3>
  );
};
...
<Route exact path="/props-through-component" component={() => <PropsPage title={`Props through component`} />} />
 */
}

export default App;
