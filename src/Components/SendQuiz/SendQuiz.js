import { useState, useEffect } from "react";
import "./SendQuiz.css";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { SERVER_URL, formatDate } from "Pages/Globals";

function SendQuiz() {
  const { quizId } = useParams();
  const history = useHistory();
  const [error, setError] = useState("");
  const [usersList, setUsersList] = useState("");

  useEffect(() => {
    const url = `${SERVER_URL}/users/getall`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    axios
      .get(url, config)
      .then((res) => {
        renderListOfUsers(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }, []);

  //{ quiz: new_quiz, questions: questionList, answers: answersList }
  function renderListOfUsers(data) {
    // console.log(data);

    let content = data.map((item, index) => (
      <option key={index} value={item.userIdentifier}>
        {" "}
        {item.userName}
      </option>
    ));

    setUsersList(content);
  }

  async function sendToRecipients(e) {
    e.preventDefault();

    const values = Object.values(e.target);

    let selectedUsers = [];
    for (let i = 0; i < values[0].options.length; i++) {
      if (values[0].options[i].selected)
        selectedUsers.push(values[0].options[i].value);
    }
    console.log("selected users: ", selectedUsers);
    await sendToServer(selectedUsers);
  }

  async function sendToServer(userList) {
    const url = `${SERVER_URL}/quizzes/send`;

    let userToken = "";

    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    let record = {};
    let res;
    for (let item of userList) {
      record = {
        recordIdentifier: {
          userIdentifier: Number(item),
          quizIdentifier: quizId,
        },
        recordMark: 0,
        recordStatus: "initial",
        recordStartDate: Date.now(),
      };

      // console.log(record);
      try {
        res = await axios.post(url, record, config);
        console.log("returned from axios: ", res);
      } catch (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      }
    }
    console.log("completed");
  }

  function handleDone() {
    history.goBack();
  }

  return (
    <div className="main">
      <h3>Select the recipients you want to send this quiz to:</h3>
      <div className="my_error">{error}</div>

      <form id="myForm" onSubmit={sendToRecipients}>
        <select multiple="multiple" id="sel">
          {usersList}
        </select>
        <input type="submit" value="Send" />
      </form>
      <button className="doneButton" type="button" onClick={handleDone}>
        Done
      </button>
    </div>
  );
}

export default SendQuiz;
