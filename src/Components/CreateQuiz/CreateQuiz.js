import { useState } from "react";
import "./CreateQuiz.css";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { SERVER_URL } from "Pages/Globals";

function CreateQuiz() {
  const [error, setError] = useState("");
  const history = useHistory();
  const [createDisabled, setCreateDisabled] = useState("");

  //{ c ? () : () }

  async function createQuiz(e) {
    e.preventDefault();
    console.log("createQuiz called");

    const values = Object.values(e.target);

    const params = {
      quizName: values[0].value,
      quizDescription: values[1].value,
      quizDeadLine: new Date(values[2].value),
      quizFormType: values[3].checked ? values[3].value : values[4].value,
    };

    console.log("calling axios", params);
    const url = `${SERVER_URL}/quizzes/create`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .post(url, params, config)
      .then((res) => {
        console.log(res.data);
        setCreateDisabled("disabled");
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }

  function handleDone() {
    history.goBack();
  }

  return (
    <div className="new_quiz">
      <h3>New Quiz</h3>

      <form onSubmit={createQuiz}>
        <div className="my_error">{error}</div>
        <div className="quizDetails">
          <label className="detailLabel">
            Quiz Name:
            <input name="name" type="string"></input>
          </label>
          <label className="detailLabel">
            Quiz Description:
            <input name="desc" type="string"></input>
          </label>
          <label className="detailLabel">
            Quiz Deadline:
            <input type="date" id="dline" name="dline"></input>
          </label>

          <label>
            Quiz Form Type:
            <input
              type="radio"
              id="test"
              name="ftype"
              value="test"
              required="required"
            ></input>
            <label htmlFor="test">Test</label>
            <input
              type="radio"
              id="survey"
              name="ftype"
              value="survey"
              required="required"
            ></input>
            <label htmlFor="survey">Survey</label>
          </label>
        </div>
        <input
          className="formButton"
          type="submit"
          value="create"
          disabled={createDisabled}
        ></input>
      </form>

      <button type="button" onClick={handleDone}>
        Done
      </button>
    </div>
  );
}
export default CreateQuiz;
