import { createContext, useState, useContext, useEffect } from "react";
import "./Header.css";
import logo from "./logo.svg";
import { LoginContext } from "Pages/Login";
import { NavLink, useHistory, useLocation } from "react-router-dom";
import { SERVER_URL } from "Pages/Globals";
import axios from "axios";

function Header() {
  const [[user, setUser]] = useContext(LoginContext);
  const history = useHistory();
  const [error, setError] = useState("");
  const [takeQuizDisabled, setTakeQuizDisabled] = useState("");
  const location = useLocation();

  useEffect(() => {
    console.log("location is: \n", location);
    if (location.pathname == "/" || location.pathname == "/quizzes-took")
      setTakeQuizDisabled("");
    else setTakeQuizDisabled("disabled");
  }, [location]);

  function logout() {
    setUser("");
    delete localStorage.quizUser;
    delete sessionStorage.quizUser;
    history.push(`/`);
  }

  function handleTakeAQuiz(e) {
    e.preventDefault();
    setError("");
    const values = Object.values(e.target);
    console.log("handleTakeAQuiz: ", values[0].value);
    if (values[0].value == "") {
      setError("type in quiz name");
      return;
    }
    const url = `${SERVER_URL}/quizzes/getIdByName/${values[0].value}`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        setTakeQuizDisabled("disabled");
        history.push(`/takequiz/${res.data}`);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`Could not find Quiz by this name`);
      });
  }

  return (
    <div>
      <div className="dashboard">
        <h1>📖The Quiz Maker</h1>
        <div className="dashboard_title">
          <div className="takeQuizAnsNav">
            <form className="quizMameForm" onSubmit={handleTakeAQuiz}>
              <div>
                <input name="quizName" type="string"></input>
                <input
                  className="takeAQuiz"
                  type="submit"
                  value="Take A Quiz"
                  disabled={takeQuizDisabled}
                ></input>
              </div>
            </form>
            <nav>
              <NavLink to="/" activeClassName="selected">
                Quizzes I Created
              </NavLink>
              <NavLink to="/quizzes-took" activeClassName="selected">
                Quizzes I Took
              </NavLink>
              <NavLink to="/quizform" activeClassName="selected">
                Create Quiz
              </NavLink>
            </nav>
          </div>
          <div className="userLogout">
            <h5>hello {user}</h5>
            <button onClick={logout}>Log Out</button>
          </div>
        </div>
      </div>
      <div className="my_error">{error}</div>
    </div>
  );
}
export default Header;
