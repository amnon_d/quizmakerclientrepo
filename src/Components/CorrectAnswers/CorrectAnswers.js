import { useState, useEffect } from "react";
import "./CorrectAnswers.css";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { SERVER_URL, formatDate } from "Pages/Globals";

function CorrectAnswers() {
  const { quizId } = useParams();
  const history = useHistory();
  const [theContent, setTheContent] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    const url = `${SERVER_URL}/quizzes/copy/${quizId}`;
    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        renderCorrectAnswers(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }, []);

  //{ quiz: new_quiz, questions: questionList, answers: answersList }
  function renderCorrectAnswers(data) {
    let len = data.questions.length;
    let correct_answers = [];
    let answersPerQuestion = [];
    for (let question of data.questions) {
      console.log("working on question: ", question.questionIdentifier);
      //data.answers is an array of arrays of answers. each external element
      //is an array of answers that belong to a specific question.
      //So first we must find the array that holds the answers for the specific question
      let selected_array = data.answers.find(
        (item) =>
          item[0].answerQuestionIdentifier == question.questionIdentifier
      );
      console.log("found a selected array: ", selected_array);
      answersPerQuestion = selected_array.filter(
        (item) => item.isAnswerCorrect
      );

      console.log("filtered answers: ", answersPerQuestion);
      correct_answers.push({
        question: question.questionDescription,
        answers: answersPerQuestion,
      });
    }
    console.log(correct_answers);

    let content = correct_answers.map((item, index) => (
      <span key={index}>
        {item.question} -
        {item.answers.map((ans, dx) => (
          <span key={dx}>{ans.answerDescription},</span>
        ))}
      </span>
    ));

    setTheContent(content);
  }

  function handleDone() {
    history.goBack();
  }

  return (
    <div className="main">
      <div className="my_error">{error}</div>

      <h3>Correct Answers for Quiz:</h3>
      <div className="data_lines">{theContent}</div>
      <button className="doneButton" type="button" onClick={handleDone}>
        Done
      </button>
    </div>
  );
}

export default CorrectAnswers;
