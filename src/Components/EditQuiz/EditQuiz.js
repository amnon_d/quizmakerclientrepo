import { useState, useEffect } from "react";
import "./EditQuiz.css";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { Link, NavLink } from "react-router-dom";
import { SERVER_URL, formatDate } from "Pages/Globals";

function EditQuiz() {
  const { quizId } = useParams();
  const [table, setTable] = useState();
  const [error, setError] = useState("");
  const [questionCounter, setQuestionCounter] = useState(1);
  const history = useHistory();
  const [quizDetails, setQuizDetails] = useState("");
  const [sendQuizDisabled, setSendQuizDisabled] = useState("disabled");
  const [addQandaDisabled, setAddQandaDisabled] = useState("");
  const [update, setUpdateDisabled] = useState("");
  const [quiz, setQuiz] = useState({});

  //{ c ? () : () }

  console.log("params passed to EditQuiz: ", quizId);

  useEffect(() => {
    const url = `${SERVER_URL}/quizzes/edit/${quizId}`;

    let userToken = "";

    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        setQuiz(res.data.quiz);
        displayQuizDetails(res.data);
        displayQuestionsDetails(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);

        setError(`${error.response.data.error}`);
      });
  }, []);

  function displayQuizDetails(data) {
    if (
      !data.quiz.isQuizActive ||
      data.quiz.quizStatus == "published" ||
      data.quiz.isQuizShared
    ) {
      setUpdateDisabled("disabled");
      setAddQandaDisabled("disabled");
      setError(
        "Quiz cannot be editted as it is either deleted, published or shared"
      );
    }

    if (data.quiz.quizStatus != "published") setSendQuizDisabled("disabled");
    else setSendQuizDisabled("");

    // let quizLinkContent = (
    //   <label className="detailLabel">
    //     Quiz Link:
    //     <input
    //       name="link"
    //       type="string"
    //       className="quizLink"
    //       // value={`http://localhost:3000${data.quiz.quizLink}`}
    //       value={`https://amnonfirst.herokuapp.com/${data.quiz.quizLink}`}
    //     ></input>
    //   </label>
    // );
    // setQuizLink(quizLinkContent);

    // we have data.quiz, data.questions and data.answers
    let quizContent = (
      <div className="quizDetails">
        <label className="detailLabel">
          Quiz Name:
          <input
            name="name"
            type="string"
            defaultValue={data.quiz.quizName}
          ></input>
        </label>
        <label className="detailLabel">
          Quiz Description:
          <input
            name="desc"
            type="string"
            defaultValue={data.quiz.quizDescription}
          ></input>
        </label>
        <label className="detailLabel">
          Quiz Deadline:
          <input
            type="date"
            id="dline"
            name="dline"
            defaultValue={formatDate(data.quiz.quizDeadLine, false)}
          ></input>
        </label>

        <label>
          Quiz Form Type:
          <input
            type="radio"
            id="test"
            name="ftype"
            value="test"
            required="required"
            defaultChecked={data.quiz.quizFormType == "test" ? "true" : ""}
          ></input>
          <label htmlFor="test">Test</label>
          <input
            type="radio"
            id="survey"
            name="ftype"
            value="survey"
            required="required"
            defaultChecked={data.quiz.quizFormType == "survey" ? "true" : ""}
          ></input>
          <label htmlFor="survey">Survey</label>
        </label>
      </div>
    );

    setQuizDetails(quizContent);
  }

  function displayQuestionsDetails(data) {
    let len = data.questions.length;
    console.log("found ", len, " qustions, so setting the counter");
    setQuestionCounter(len + 1);
    let content = data.questions.map((item, index) => (
      <tbody key={index}>
        <tr>
          <td>
            <Link
              to={`/editqanda/${item.questionQuizIdentifier}/${item.questionIdentifier}`}
            >
              {item.questionName}
            </Link>
          </td>
          <td>{item.questionType}</td>
          <td>{item.isQuestionMandatory ? <>&#10003;</> : <>&#10005;</>}</td>
          <td>{item.questionPoints}</td>
        </tr>
      </tbody>
    ));

    setTable(content);
  }

  async function editQuiz(e) {
    e.preventDefault();

    const values = Object.values(e.target);
    console.log("editQuiz called: ", values);

    // search for the confirmation checkbox to determine if to publish or not
    let publish = false;
    for (let value of values) {
      if (
        value.type == "checkbox" &&
        value.checked &&
        value.name == "cofirmation_checkbox"
      ) {
        publish = value.checked;
        setSendQuizDisabled("");
        break;
      }
    }

    const params = {
      quizIdentifier: quizId,
      quizName: values[0].value,
      quizDescription: values[1].value,
      quizDeadLine: new Date(values[2].value),
      quizFormType: values[3].checked ? values[3].value : values[4].value,
      quizStatus: publish ? "published" : "draft",
    };

    console.log("calling axios", params);
    const url = `${SERVER_URL}/quizzes/update`;

    let userToken = "";

    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .post(url, params, config)
      .then((res) => {
        console.log(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }

  function handleDone() {
    history.push(`/`);
  }

  return (
    <div className="new_quiz">
      <h3>Edit Quiz</h3>

      <NavLink to={`/send_quiz/${quizId}`} activeClassName="selected">
        <button
          type="button"
          className="sendQuizButton"
          disabled={sendQuizDisabled}
        >
          Send Quiz to Recipients
        </button>
      </NavLink>

      <form onSubmit={editQuiz}>
        <div className="my_error">{error}</div>
        {quizDetails}
        <label className="cofirmation_checkbox">
          check if you want to publish your quiz. <br></br> Once published, it
          cannot be editted again
          <div>
            <input name="cofirmation_checkbox" type="checkbox"></input>
          </div>
        </label>
        <input
          className="formButton"
          type="submit"
          value="Update"
          disabled={update}
        ></input>
      </form>

      <div>
        <h3>Questions List</h3>
        <table className="table">
          <thead>
            <tr>
              <th>Question Name</th>
              <th>Question Type</th>
              <th>Is Mandatory</th>
              <th>Points</th>
            </tr>
          </thead>
          {table}
        </table>
      </div>

      <NavLink
        to={`/createqanda/${quizId}/${quizId}QS${questionCounter}`}
        activeClassName="selected"
      >
        <button type="button" className="addButton" disabled={addQandaDisabled}>
          Add QandA
        </button>
      </NavLink>

      <button className="doneButton" type="button" onClick={handleDone}>
        Done
      </button>
    </div>
  );
}
export default EditQuiz;
