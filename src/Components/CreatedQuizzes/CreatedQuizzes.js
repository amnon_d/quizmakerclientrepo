import { useEffect, useState } from "react";
import "./CreatedQuizzes.css";
import axios from "axios";
import { Link, NavLink } from "react-router-dom";
import { SERVER_URL, formatDate } from "Pages/Globals";

function CreatedQuizzes() {
  const [table, setTable] = useState();
  const [error, setError] = useState();

  //{ c ? () : () }
  useEffect(() => {
    const url = `${SERVER_URL}/quizzes/getCreatedByUser`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    console.log(userToken);
    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        displayCreatedQuizzes(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }, []);

  function displayCreatedQuizzes(data) {
    let content = data.map((item, index) => (
      <tbody key={index}>
        <tr>
          <td>
            <Link to={`/quizform/${item.quizIdentifier}`}>{item.quizName}</Link>
          </td>
          <td>{formatDate(item.quizDeadLine)}</td>
          <td>{item.quizStatus}</td>
          <td>{formatDate(item.quizCreationDate)}</td>
          <td>{item.quizFormType}</td>
        </tr>
      </tbody>
    ));

    setTable(content);
  }

  return (
    <div className="created_quizzes">
      <div>
        <div className="my_error">{error}</div>

        <h3>Quizzes I created</h3>
        <NavLink to="/quizform" activeClassName="selected">
          <button type="button" className="plusButton">
            +
          </button>
        </NavLink>
      </div>
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Quiz name</th>
              <th>Deadline</th>
              <th>Status</th>
              <th>Creation Date</th>
              <th>Type</th>
            </tr>
          </thead>
          {table}
        </table>
      </div>
    </div>
  );
}
export default CreatedQuizzes;
