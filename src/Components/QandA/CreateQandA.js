import { useState } from "react";
import "./QandA.css";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import { SERVER_URL } from "Pages/Globals";

function CreateQandA() {
  const { quizId, questionId } = useParams();
  const history = useHistory();
  const [error, setError] = useState("");
  const [questionName, setQuestionName] = useState("question");
  const [saveDisabled, setSaveDisabled] = useState("");

  // I initialized with an empty component because for some reason the first one was
  // more to the right than the rest.
  // so in the createQuestionAndAnswers function I had to remember that the actual length of the list
  // is one less than answerList.length.
  const [answersList, setAnswersList] = useState([<> </>]);

  if (quizId)
    console.log("params passed to CreateQandA: ", quizId, "   ", questionId);
  else console.log("no params passed");

  //{ c ? () : () }

  async function createQuestionAndAnswers(e) {
    e.preventDefault();
    console.log("createQuestionAndAnswers called");

    const values = Object.values(e.target);

    // console.log(values);

    let question = {
      questionIdentifier: `${questionId}`,
      questionName: values[0].value,
      questionDescription: values[1].value,
      questionType: values[2].checked ? values[2].value : values[3].value,
      questionQuizIdentifier: `${quizId}`,
      isQuestionMandatory: values[4].checked,
      questionPoints: values[5].value,
    };

    let numOfCorrectAnswers = question.questionType == "singleAnswer" ? 1 : 2;

    //set the first index for the answers values
    let idx = 6;
    // see the note on the useState of this state
    const ans_len = answersList.length - 1;
    let answers = [];
    let answer = {};
    let checkedAnswersCount = 0;
    for (let i = 0; i < ans_len; i++) {
      answer = {
        answerIdentifier: `${questionId}ANS${i + 1}`,
        answerDescription: values[idx++].value,
        isAnswerCorrect: values[idx++].checked,
        answerQuestionIdentifier: `${questionId}`,
      };
      answers.push(answer);
      // advance the idx value because the next value belongs to the "remove" button on each answer
      idx++;
      checkedAnswersCount =
        answer.isAnswerCorrect == true
          ? checkedAnswersCount + 1
          : checkedAnswersCount;
    }

    console.log("The question: \n", question);
    console.log("The answers: \n", answers);

    if (checkedAnswersCount != numOfCorrectAnswers && ans_len !== 0) {
      setError(
        "Number of correct answers does not correspond with the Question Type"
      );
      return;
    } else {
      setError("");
    }

    const params = { question: question, answers: answers };

    console.log("calling axios", params);
    const url = `${SERVER_URL}/questions/create_qanda`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .post(url, params, config)
      .then((res) => {
        console.log(res.data);
        setSaveDisabled("disabled");
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }

  function handleDone() {
    history.goBack();
  }

  function onQchange(e) {
    e.preventDefault();

    const value = Object.values(e.target.value);
    setQuestionName(value);
  }

  let addAnswer = (i) => {
    // the i number is insignificant for the logic here.
    // It is just so I can number the answers for the display
    setAnswersList([
      ...answersList,
      <>
        <label>
          {`${i}`}.Answer Description:
          <input name="adesc" type="string"></input>
        </label>
        <label>
          is Correct:
          <input name="correct" type="checkbox"></input>
        </label>
      </>,
    ]);
  };

  let removeAnswer = (i) => {
    let newAnswerList = [...answersList];
    newAnswerList.splice(i, 1);
    setAnswersList(newAnswerList);
  };

  return (
    <div className="qanda">
      <h3>{questionName}</h3>
      <form onSubmit={createQuestionAndAnswers}>
        <div className="my_error">{error}</div>
        <div className="qandaDetails"></div>
        <label className="detailLabel">
          Question Name:
          <input name="name" type="string" onChange={onQchange}></input>
        </label>
        <label className="detailLabel">
          Question Description:
          <input name="desc" type="string"></input>
        </label>

        <label>
          Question Type:
          <input
            type="radio"
            id="single"
            name="qstype"
            value="singleAnswer"
            required="required"
          ></input>
          <label for="single">Single Answer</label>
          <input
            type="radio"
            id="multi"
            name="qstype"
            value="multipleAnswer"
            required="required"
          ></input>
          <label for="multi">Two Answers</label>
        </label>

        <label>
          is Mandatory:
          <input name="mandatory" type="checkbox"></input>
        </label>

        <label className="detailLabel">
          Question Points:
          <input name="points" type="number"></input>
        </label>

        <h3>Answers</h3>
        <div className="answers">
          {answersList.map((element, index) => (
            <div className="form-inline" key={index}>
              {element}
              {index ? (
                <button
                  type="button"
                  className="button remove"
                  onClick={() => removeAnswer(index)}
                >
                  Remove
                </button>
              ) : null}
            </div>
          ))}
          <button
            className="button add"
            type="button"
            onClick={() => addAnswer(answersList.length)}
          >
            Add
          </button>
        </div>

        <input type="submit" value="save" disabled={saveDisabled}></input>
      </form>

      <button type="button" onClick={handleDone}>
        Done
      </button>
    </div>
  );
}
export default CreateQandA;
