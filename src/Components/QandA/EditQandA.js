import { useState, useEffect } from "react";
import "./QandA.css";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import { SERVER_URL } from "Pages/Globals";

function EditQandA() {
  const { quizId, questionId } = useParams();
  const history = useHistory();
  const [error, setError] = useState("");
  const [saveDisabled, setSaveDisabled] = useState("");

  // I initialized with an empty component because for some reason the first one was
  // more to the right than the rest.
  // so in the EditQandA function I had to remember that the actual length of the list
  // is one less than answerList.length.
  const [answersList, setAnswersList] = useState([<> </>]);

  const [questionDetails, setQuestionDetails] = useState("");
  const [answersDetails, setAnswersDetails] = useState("");
  if (quizId)
    console.log("params passed to EditQandA: ", quizId, "   ", questionId);
  else console.log("no params passed");

  //{ c ? () : () }

  useEffect(() => {
    const url = `${SERVER_URL}/questions/edit/${questionId}`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        renderQuestion(res.data);
        renderAnswers(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }, []);

  function renderQuestion(data) {
    console.log(data.question);
    // we have data.question, and data.answers
    let questionContent = (
      <div className="qandaDetails">
        <label className="detailLabel">
          Question Name:
          <input
            name="name"
            type="string"
            defaultValue={data.question.questionName}
          ></input>
        </label>
        <label className="detailLabel">
          Question Description:
          <input
            name="desc"
            type="string"
            defaultValue={data.question.questionDescription}
          ></input>
        </label>

        <label>
          Question Type:
          <input
            type="radio"
            id="single"
            name="qstype"
            value="singleAnswer"
            required="required"
            defaultChecked={
              data.question.questionType == "singleAnswer" ? "true" : ""
            }
          ></input>
          <label htmlFor="single">Single Answer</label>
          <input
            type="radio"
            id="multi"
            name="qstype"
            value="multipleAnswer"
            required="required"
            defaultChecked={
              data.question.questionType == "multipleAnswer" ? "true" : ""
            }
          ></input>
          <label htmlFor="multi">Two Answers</label>
        </label>

        <label>
          is Mandatory:
          <input
            name="mandatory"
            type="checkbox"
            defaultChecked={data.question.isQuestionMandatory}
          ></input>
        </label>

        <label className="detailLabel">
          Question Points:
          <input
            name="points"
            type="number"
            defaultValue={data.question.questionPoints}
          ></input>
        </label>
      </div>
    );

    setQuestionDetails(questionContent);
  }

  function renderAnswers(data) {
    console.log(data.answers);
    let counter = 1;
    // we have data.question, and data.answers
    let answersContent = data.answers.map((element, index) => (
      <>
        <label>
          {`${counter++}`}.Answer Description:
          <input
            name="adesc"
            type="string"
            defaultValue={element.answerDescription}
          ></input>
        </label>
        <label>
          is Correct:
          <input
            name="correct"
            type="checkbox"
            defaultChecked={element.isAnswerCorrect}
          ></input>
        </label>
      </>
    ));

    setAnswersList([<> </>, ...answersContent]);
  }

  async function editQuestionAndAnswers(e) {
    e.preventDefault();
    console.log("editQuestionAndAnswers called");

    console.log(e);

    const values = Object.values(e.target);

    console.log(values);
    console.log("sizeod list: ", answersList.length);

    let question = {
      questionIdentifier: `${questionId}`,
      questionName: values[0].value,
      questionDescription: values[1].value,
      questionType: values[2].checked ? values[2].value : values[3].value,
      questionQuizIdentifier: `${quizId}`,
      isQuestionMandatory: values[4].checked,
      questionPoints: values[5].value,
    };

    let numOfCorrectAnswers = question.questionType == "singleAnswer" ? 1 : 2;

    //set the first index for the answers values
    let idx = 6;
    // see the note on the useState of this state
    const ans_len = answersList.length - 1;
    let answers = [];
    let answer = {};
    let checkedAnswersCount = 0;
    for (let i = 0; i < ans_len; i++) {
      answer = {
        answerIdentifier: `${questionId}ANS${i + 1}`,
        answerDescription: values[idx++].value,
        isAnswerCorrect: values[idx++].checked,
        answerQuestionIdentifier: `${questionId}`,
      };
      answers.push(answer);
      // advance the idx value because the next value belongs to the "remove" button on each answer
      idx++;
      checkedAnswersCount =
        answer.isAnswerCorrect == true
          ? checkedAnswersCount + 1
          : checkedAnswersCount;
    }

    console.log("The question: \n", question);
    console.log("The answers: \n", answers);

    // console.log("Another way to read the list from the form\n\n")
    // const my_values = Object.values(e.target).reduce(
    //   (acc, input) =>
    //     !input.name
    //       ? acc
    //       : {
    //           ...acc,
    //           [input.name]:
    //             input.type == "checkbox" || "radio"? input.checked : input.value,
    //         },
    //   {}
    // );
    // console.log(my_values);

    if (checkedAnswersCount != numOfCorrectAnswers && ans_len !== 0) {
      setError(
        "Number of correct answers does not correspond with the Question Type"
      );
      return;
    } else {
      setError("");
    }

    const params = { question: question, answers: answers };

    console.log("calling axios", params);
    // we disable the save button because if the user double clicks on the button
    // it will cause errors. The reason for this is that when updating QansA the
    //server deletes the old and writes the new (simpler logic than trying to work out
    // what exactly has changed)
    setSaveDisabled("disabled");
    const url = `${SERVER_URL}/questions/update_qanda`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .post(url, params, config)
      .then((res) => {
        console.log(res.data);
        setSaveDisabled("");
      })
      .catch(function (error) {
        console.log(error);
        setSaveDisabled("");
        // setError("Failed to save the information to the server");
        setError(`${error.response.data.error}`);
      });
  }

  function handleDone() {
    history.goBack();
  }

  // decided not to use
  // function onQchange(e) {
  //   e.preventDefault();

  //   const value = Object.values(e.target.value);
  //   // console.log(e);
  //   setQuestionName(value);
  // }

  let addAnswer = (i) => {
    // the i number is insignificant for the logic here.
    // It is just so I can number the answers for the display
    setAnswersList([
      ...answersList,
      <>
        <label>
          {`${i}`}.Answer Description:
          <input name="adesc" type="string"></input>
        </label>
        <label>
          is Correct:
          <input name="correct" type="checkbox"></input>
        </label>
      </>,
    ]);
  };

  let removeAnswer = (i) => {
    let newAnswerList = [...answersList];
    newAnswerList.splice(i, 1);
    setAnswersList(newAnswerList);
  };

  return (
    <div className="qanda">
      <h3>Edit Question</h3>
      <form onSubmit={editQuestionAndAnswers}>
        <div className="my_error">{error}</div>

        {questionDetails}

        <h3>Edit Answers</h3>
        <div className="answers">
          {answersList.map((element, index) => (
            <div className="form-inline" key={index}>
              {element}
              {index ? (
                <button
                  type="button"
                  className="button remove"
                  onClick={() => removeAnswer(index)}
                >
                  Remove
                </button>
              ) : null}
            </div>
          ))}
          <button
            className="button add"
            type="button"
            onClick={() => addAnswer(answersList.length)}
          >
            Add
          </button>
        </div>

        <input type="submit" value="save" disabled={saveDisabled}></input>
      </form>
      <button className="formButton" type="button" onClick={handleDone}>
        Done
      </button>
    </div>
  );
}
export default EditQandA;
