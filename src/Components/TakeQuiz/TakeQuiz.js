import { useState, useEffect, useContext } from "react";
import "./TakeQuiz.css";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { SERVER_URL, formatDate } from "Pages/Globals";
import { LoginContext } from "Pages/Login";

//This is a version that prints a certificate.
//I will not continue to develop this version because I did not succeed
// to refresh the comppnent and return to the quiz after the printing
//and the display stayed on the certificate until I manualy press refresh
// Anyhow, this component here is the same like the Login and Register in
// the way that they do not cpome under the header. They all use mode state
// and they switch between them by setting the mode.
// Also for this component, the parent component (or the component that switched
// the mode to make this one appear), had to use the context from Login in order
//to set the mode and most important to store the data required for the certificate
// somewhere where the Certificate component can read it from, and that is the Login.
// This component (Certificate) is called from Login and it is wrapped with Provider
// of the context because it is not a child component of Login, and it had to
//read the data needed for the certificate from the Login.
// Regarding the sensitivity of the "Print Certificate" button, it is active only
// if the quiz was already submitted (in which case the save button is greyed out)
// either it was submitted in the past or just now after clicking the save button
//with submit checkbox ticked
//P.S. I solved the problem of refresh. The problem was that Login, Register and
//Certificate  components were using a separate useState for the mode. Now Login
//is the only component that holds the mode state and it provides its context
//explicitly to the other components.

function TakeQuiz() {
  const { quizId } = useParams();

  const [error, setError] = useState("");

  const history = useHistory();

  const [quizDetails, setQuizDetails] = useState("");
  const [quizContent, setQuizContent] = useState("");
  const [saveDisabled, setSaveDisabled] = useState("");
  const [mark, setMark] = useState("");
  const [questionList, setQuestionList] = useState([]);
  const [theQuiz, setTheQuiz] = useState();
  const [loginC, [mode, setMode], [certificateData, setCertificateData]] =
    useContext(LoginContext);
  const [printCertificateDisabled, setPrintCertificateDisabled] =
    useState("disabled");
  const [viewCorrectAnswersDisabled, setViewCorrectAnswersDisabled] =
    useState("disabled");

  //{ c ? () : () }

  if (quizId) console.log("quizId params passed to TakeQuiz: ", quizId);
  else console.log("no quizId params passed");

  // the server will provide :
  //{ quiz: res, questions: questionList, answers: answersList }
  // answers will be missing the isCorrect fields

  useEffect(() => {
    const url = `${SERVER_URL}/quizzes/open/${quizId}`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        //save the question list for later to check that the number of marked answers
        console.log("quiz is: ", res.data.quiz);
        // corresponds with the question requirments
        setQuestionList(res.data.questions);
        renderQuiz(res.data);
        renderQuestions(res.data);
        if (res.data.record.recordStatus == "submitted") {
          setCertificateData({
            record: res.data.record,
            quizName: res.data.quiz.quizName,
          });
          setPrintCertificateDisabled("");
          setSaveDisabled("disabled");
          setViewCorrectAnswersDisabled("");
        }
        if (hasDeadlinePassed(res.data.quiz)) {
          setError("Sorry, Submission date for this quiz has expired");
          setSaveDisabled("disabled");
          return;
        }
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }, []);

  function hasDeadlinePassed(quiz) {
    let now = new Date();
    console.log(quiz.quizDeadLine);
    let deadline = new Date(quiz.quizDeadLine);
    setTheQuiz(quiz);
    return now > deadline ? true : false;
  }

  function renderQuiz(data) {
    // we have data.quiz, data.questions,  data.answers and data.record
    let content = (
      <div className="quizDetails">
        <div className="detailLine">
          <div>Quiz Name:</div>
          <div>{data.quiz.quizName}</div>
        </div>
        <div className="detailLine">
          <div>Deadline:</div>
          <div> {formatDate(data.quiz.quizDeadLine)}</div>
        </div>
        <div className="quizTitle">{data.quiz.quizDescription}</div>
      </div>
    );

    setQuizDetails(content);
  }

  function renderQuestions(data) {
    //extract all marked answers from data.record and put them in a flat array (one dimention array)
    // this way it will be easier to work with later
    console.log("record is ", data.record);

    let marked_answers = [];
    for (let item1 of data.record.recordAnswers) {
      console.log(item1);
      for (let item2 of item1.answers) {
        console.log(item2);
        marked_answers.push(item2);
      }
    }

    console.log("marked answers: \n", marked_answers);

    //first create a list of all questions and their related answers
    let joinedList = [];
    let match = false;
    let marked;
    for (let i in data.questions) {
      console.log("doing Q ", data.questions[i].questionIdentifier);
      for (let a of data.answers) {
        if (
          a.length != 0 &&
          a[0].answerQuestionIdentifier == data.questions[i].questionIdentifier
        ) {
          console.log("match found");
          match = true;
          let ans_arr = [];
          for (let ans of a) {
            //see if this answer was marked earlier by this user
            marked = marked_answers.find(
              (item) => item == ans.answerIdentifier
            );
            ans_arr.push(
              <label key={ans.answerIdentifier}>
                <input
                  name="ans"
                  type="checkbox"
                  q_id={ans.answerQuestionIdentifier}
                  a_id={ans.answerIdentifier}
                  defaultChecked={marked ? true : false}
                ></input>
                {ans.answerDescription}
              </label>
            );
          }
          joinedList.push({
            question: (
              <h5>
                {data.questions[i].questionType == "singleAnswer"
                  ? data.questions[i].questionDescription +
                    `-(${data.questions[i].questionPoints} points)`
                  : data.questions[i].questionDescription +
                    `-(${data.questions[i].questionPoints} points)` +
                    "   two answers required"}
                {data.questions[i].isQuestionMandatory
                  ? ""
                  : " - Not Mandatory"}
              </h5>
            ),
            answers: ans_arr,
          });
        }
        if (match) {
          match = false;
          break;
        }
      }
    }
    console.log(joinedList);

    let content = joinedList.map((element, index) => (
      <div key={index}>
        {element.question} {element.answers}
      </div>
    ));

    setQuizContent(content);
  }

  //This should call saveQuizSolutions on Quizzes save_solutions
  //{quizIdentifier, solutions{[{questionIdentifier, answers[]}] ,submit{bool}}
  async function saveQuiz(e) {
    e.preventDefault();

    if (hasDeadlinePassed(theQuiz)) {
      setError("Sorry, Submission date for this quiz has expired");
      setSaveDisabled("disabled");
      return;
    }

    const values = Object.values(e.target);
    console.log("takeQuiz called, ", values);
    console.log(values[0].type);

    console.log(values[0].attributes.q_id.value);
    console.log(values[0].attributes.a_id.value);

    let solutions = [];
    let index;
    let submit = false;
    for (let value of values) {
      //some questions has multiple answers so we need to handle the result with this in mind
      //search for an item in the array of solutions and if not found one - create it.
      // if found one, just add the specific data (the answer id) to it.
      if (value.type == "checkbox" && value.checked) {
        if (value.name == "cofirmation_checkbox") submit = value.checked;
        else {
          index = solutions.findIndex(
            (item) => item.questionIdentifier == value.attributes.q_id.value
          );
          if (index == -1) {
            let answers = [];
            answers.push(value.attributes.a_id.value);
            solutions.push({
              questionIdentifier: value.attributes.q_id.value,
              answers: answers,
            });
          } else {
            solutions[index].answers.push(value.attributes.a_id.value);
          }
        }
      }
    }

    //check that the number of marked answers corresponds with the question requirments
    let numberOfMarkedAnswers = 0;
    let questionInCheck;
    let numberOfRequiredAnswers;
    for (let solution of solutions) {
      numberOfMarkedAnswers = solution.answers.length;
      questionInCheck = questionList.find(
        (item) => item.questionIdentifier == solution.questionIdentifier
      );
      numberOfRequiredAnswers =
        questionInCheck.questionType == "singleAnswer" ? 1 : 2;
      if (numberOfMarkedAnswers != numberOfRequiredAnswers) {
        setError(
          `The number of marked answers for question ${questionInCheck.questionName} does not correspond with the requirements`
        );
        return;
      }
    }

    // now we need to pack the params
    const params = {
      quizIdentifier: quizId,
      solutions: solutions,
      submit: submit,
    };

    console.log("calling axios\n", solutions);
    const url = `${SERVER_URL}/quizzes/save_solutions`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };
    axios
      .post(url, params, config)
      .then((res) => {
        console.log("Your mark: ", res.data.markText);

        if (submit) {
          // we set the info for the Certificate component in the Login component's context
          // so the Certificate component will take it from there.
          setCertificateData({
            record: res.data.record,
            quizName: theQuiz.quizName,
          });
          setPrintCertificateDisabled("");

          setSaveDisabled("disabled");
          setMark(`Your mark: ${res.data.markText}`);
        }
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }

  function handleDone() {
    history.goBack();
  }

  function handlePrintCertificate() {
    setMode("certificate");
  }

  function handleViewCorrectAnswers() {
    console.log("ViewCorrectAnswers called");
    history.push(`/correct_answers/${theQuiz.quizIdentifier}`);
  }

  return (
    <div className="new_quiz">
      <h3>Welcome to The Quiz</h3>
      {quizDetails}
      <form onSubmit={saveQuiz}>
        <div className="my_error">{error}</div>

        {quizContent}

        <label className="cofirmation_checkbox">
          check if you want to submit your quiz. <br></br>If you do, you will
          not be able to submit it again
          <div>
            <input name="cofirmation_checkbox" type="checkbox"></input>
          </div>
        </label>

        <input type="submit" value="save" disabled={saveDisabled}></input>
      </form>

      <div className="btn_and_mark">
        <button type="button" className="doneButton" onClick={handleDone}>
          Done
        </button>

        <label className="mark">{mark}</label>
        <button
          type="button"
          className="printCertificateButton"
          onClick={handlePrintCertificate}
          disabled={printCertificateDisabled}
        >
          Print Certificate
        </button>
        <span className="return_instructions">
          click the certificate image to return here
        </span>
        <button
          type="button"
          className="correctAnswers"
          onClick={handleViewCorrectAnswers}
          disabled={viewCorrectAnswersDisabled}
        >
          Correct Answers
        </button>
      </div>
    </div>
  );
}

export default TakeQuiz;
