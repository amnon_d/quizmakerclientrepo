import { useEffect, useState } from "react";
import "./TookQuizzes.css";
import axios from "axios";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { SERVER_URL, formatDate } from "Pages/Globals";

function TookQuizzes() {
  const [table, setTable] = useState();
  const [get_submitted, setGetSubmitted] = useState(false);
  const [get_notsubmitted, setGetNotSubmitted] = useState(false);
  const [error, setError] = useState();

  //{ c ? () : () }

  useEffect(() => {
    const url = `${SERVER_URL}/records/getRcAndQzTakenByUser`;

    let userToken = "";
    if (localStorage.quizUser)
      userToken = JSON.parse(localStorage.quizUser).token;
    else if (sessionStorage.quizUser)
      userToken = JSON.parse(sessionStorage.quizUser).token;
    else {
      setError(
        "You did not choose to stay connected. Please logout and login again."
      );
      return;
    }

    const config = { headers: { authorization: userToken } };

    console.log(userToken);
    axios
      .get(url, config)
      .then((res) => {
        console.log(res.data);
        displayTakenQuizzes(res.data);
      })
      .catch(function (error) {
        console.log(error.response.data.error);
        setError(`${error.response.data.error}`);
      });
  }, [get_submitted, get_notsubmitted]);

  function filterTakenQuizzes(data) {
    console.log("getsubmitted: ", get_submitted);
    console.log("getnotsubmitted: ", get_notsubmitted);

    let filtered = data.filter((item) => {
      if (
        (get_submitted && item.record.recordStatus == "submitted") ||
        (get_notsubmitted && item.record.recordStatus != "submitted")
      ) {
        console.log("ACCEPTING");
        return item;
      } else {
        console.log("not accepting");
      }
    });

    return filtered;
  }

  function displayTakenQuizzes(data) {
    const filtered = filterTakenQuizzes(data);

    let content = filtered.map((item, index) => (
      <tbody key={index}>
        <tr>
          <td>
            <Link to={`/takequiz/${item.quiz.quizIdentifier}`}>
              {item.quiz.quizName}
            </Link>
          </td>
          <td>{item.quiz.quizFormType}</td>
          <td>{item.record.recordStatus}</td>
          <td>
            {item.record.recordStatus == "submitted"
              ? item.record.recordMark
              : ""}
          </td>
          <td>
            {item.record.recordStatus == "submitted"
              ? ""
              : formatDate(item.quiz.quizDeadLine)}
          </td>
        </tr>
      </tbody>
    ));
    setTable(content);
  }

  function toggleSubmitted() {
    console.log("toggleSubmitted called");
    setGetSubmitted(!get_submitted);
  }

  function toggleNotSubmitted() {
    console.log("toggleNotSubmitted called");
    setGetNotSubmitted(!get_notsubmitted);
  }

  return (
    <div className="took_quizzes">
      <h3>Quizzes I took</h3>
      <div className="my_error">{error}</div>
      <label>
        <input
          type="checkbox"
          onChange={toggleSubmitted}
          checked={get_submitted}
        ></input>
        Submitted
      </label>
      <label>
        <input
          type="checkbox"
          onChange={toggleNotSubmitted}
          checked={get_notsubmitted}
        ></input>
        Not Submitted
      </label>
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Quiz name</th>
              <th>Type</th>
              <th>Status</th>
              <th>Mark</th>
              <th>Deadline</th>
            </tr>
          </thead>
          {table}
        </table>
      </div>
    </div>
  );
}
export default TookQuizzes;
